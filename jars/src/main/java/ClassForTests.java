import org.eustrosoft.contractpkg.Model.*;
import org.eustrosoft.contractpkg.Controller.*;
import org.eustrosoft.contractpkg.zcsv.*;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLOutput;


public class ClassForTests {

    public static final String DEFAULT_TESTDB_ROOT="./src/test/resources/";
    public static final String TESTINGDATA_CSV="testingData.csv";


    public static String getTestDBFileName(String file_name){ return(DEFAULT_TESTDB_ROOT + file_name); }
    public static String getTestDBFileName(){return(getTestDBFileName("")); }

    private static String[] namesMap = new String[]
            {"ZRID", "ZVER", "ZDATE", "ZUID", "ZSTA", "QR  код", "№ договора", "Дата договора",
                    "Деньги по договору", "Юр-лицо поставщик", "Юр-лицо клиент", "Тип продукта", "Модель продукта",
                    "SN", "Дата производства", "Дата ввоза (ГТД)",
                    "Дата продажи", "Дата отправки клиенту", "Дата начала гарантии",
                    "Дата окончания гарантии", "Комментарий (для клиента)"};

    private final static String DELETED_RECORD_STATUS = "D";

    public static void main(String[] args) throws Exception {
        /*ZCSVFile zcsvFile;
        String member = "EXAMPLESD", range = "0100A";
        String rootPath = "/s/qrdb/EXAMPLESD/members/" + member + "/" + range + "/";
        zcsvFile = setupZCSVPaths(rootPath, "master.list.csv");

        System.out.println(zcsvFile.getFileRowsLength());
        System.out.println(zcsvFile.toString());
        if (Files.exists(Paths.get(zcsvFile.toString()))) { //SIC! if(!..) then err is better
            if (zcsvFile.tryOpenFile(1)) {
                zcsvFile.loadFromFileValidVersions();
                for (int i = zcsvFile.getFileRowsLength() - 1; i >= 0; i--) {
                    ZCSVRow eachRow = zcsvFile.getRowObjectByIndex(i);
                    eachRow.setNames(namesMap);

                    for (int j = 0; j < eachRow.getNames().length; j++) {
                        String wroteString = eachRow.get(j);
                        System.out.print(wroteString + " ");
                    }
                    System.out.println();

                }
            }
        }*/
        ZCSVFile testingFile = new ZCSVFile();
        testingFile.setRootPath("/home/yadzuka/workspace/programming/Java_projects/Sources/Java_product_projects/edit.qr.qxyz.ru/jars/src/test/resources/");
        testingFile.setFileName(TESTINGDATA_CSV);
        System.out.println(testingFile.getFileName());
        try {
            testingFile.tryOpenFile(0);
            testingFile.loadFromFileValidVersions();
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader
                            (new FileInputStream(getTestDBFileName(TESTINGDATA_CSV))
                                    , StandardCharsets.UTF_8));

            ZCSVRow row = testingFile.getRowObjectByIndex(0);
            System.out.println(row.toString());
            for(int i=0;i<testingFile.getFileRowsLength();i++)
                System.out.println(testingFile.getRowObjectByIndex(i));

        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            try {
                testingFile.closeFile();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }

    private static ZCSVFile setupZCSVPaths(String rootPath, String fileName) {
        ZCSVFile file = new ZCSVFile();
        file.setRootPath(rootPath);
        file.setFileName(fileName);
        return file;
    }

}
